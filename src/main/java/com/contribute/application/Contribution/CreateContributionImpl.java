package com.contribute.application.Contribution;

import com.contribute.controller.Request;
import com.contribute.domain.Contribution;
import com.contribute.domain.ICreateContribution;
import com.contribute.infrastructure.ContributionsDTO;
import com.contribute.infrastructure.IRepository;
import org.springframework.stereotype.Service;

@Service
public class CreateContributionImpl implements ICreateContribution {
  private final IRepository repository;

  public CreateContributionImpl(IRepository repository) {
    this.repository = repository;
  }

  public void save(Request content) {
    try{
      Contribution contribution = new Contribution(content);
      repository.create(contribution);
    }catch (Exception exception){
      throw exception;
    }
  }

  public ContributionsDTO retrieveContributions() {
    try{
      return repository.findAll();
    }catch (Exception exception){
      throw exception;
    }
  }
}
