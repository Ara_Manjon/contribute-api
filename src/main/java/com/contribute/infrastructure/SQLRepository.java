package com.contribute.infrastructure;

import com.contribute.domain.Contribution;
import com.contribute.domain.ContributionDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Component;
import org.springframework.jdbc.core.JdbcTemplate;

@Component
public class SQLRepository implements IRepository {

  @Autowired
  JdbcTemplate jdbcTemplate;

  @Override
  public void create(Contribution contribution) {
      jdbcTemplate.update("INSERT INTO contributions (content) VALUES (?)", contribution.getContent());
  }

  public ContributionsDTO findAll() {
    ContributionsDTO contributionsDTO = new ContributionsDTO();
    try {
      jdbcTemplate.query("SELECT * FROM contributions", new Object[]{}, (rs, rowNum) -> {
        ContributionDTO contributionDTO = new ContributionDTO();
        contributionDTO.content = rs.getString("content");
        contributionsDTO.add(contributionDTO);
            return null;
          });
      return contributionsDTO;

    } catch (EmptyResultDataAccessException e) {
      return null;
    }
  }
}
