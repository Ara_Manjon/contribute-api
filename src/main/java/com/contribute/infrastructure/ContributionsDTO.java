package com.contribute.infrastructure;

import com.contribute.domain.ContributionDTO;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class ContributionsDTO {
  public List<ContributionDTO> contributionDTOList;

  public ContributionsDTO() {
    this.contributionDTOList = new ArrayList<>();
  }

  public void add(ContributionDTO contributionDTO) {
    contributionDTOList.add(contributionDTO);
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    ContributionsDTO that = (ContributionsDTO) o;
    return Objects.equals(contributionDTOList, that.contributionDTOList);
  }

  @Override
  public int hashCode() {
    return Objects.hash(contributionDTOList);
  }

  @Override
  public String toString() {
    return "ContributionsDTO{" +
        "contributionDTOList=" + contributionDTOList +
        '}';
  }
}
