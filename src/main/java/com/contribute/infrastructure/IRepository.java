package com.contribute.infrastructure;

import com.contribute.domain.Contribution;

public interface IRepository {
  void create(Contribution contribution);

  ContributionsDTO findAll();
}
