package com.contribute.controller.Contribution;

import com.contribute.controller.Request;
import com.contribute.application.Contribution.CreateContributionImpl;
import com.contribute.domain.ICreateContribution;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@EnableScheduling
@Controller
public class ContributionController {

  private final ICreateContribution createContributionImpl;

  public ContributionController(CreateContributionImpl createContributionImpl) {
    this.createContributionImpl = createContributionImpl;
  }

  @PostMapping(value = "/contribution", consumes = "application/json")
  public ResponseEntity<Object> save(@RequestBody Request request) {
    try{
      createContributionImpl.save(request);
      return new ResponseEntity<>("Contribution created successfully", HttpStatus.CREATED);
    }catch(RuntimeException exception){
      return new ResponseEntity<>(exception.getMessage(), HttpStatus.SERVICE_UNAVAILABLE);
    }
  }

  @GetMapping("/contributions")
  public ResponseEntity<Object> retrieveContributions() {
    try{
      return new ResponseEntity<>(createContributionImpl.retrieveContributions(), HttpStatus.OK);
    }catch(RuntimeException exception){
      return new ResponseEntity<>(exception.getMessage(), HttpStatus.SERVICE_UNAVAILABLE);
    }
  }
}
