package com.contribute.domain;

import java.util.Objects;

public class ContributionDTO {
  public String content;

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    ContributionDTO that = (ContributionDTO) o;
    return Objects.equals(content, that.content);
  }

  @Override
  public int hashCode() {
    return Objects.hash(content);
  }

  @Override
  public String toString() {
    return "ContributionDTO{" +
        "content='" + content + '\'' +
        '}';
  }
}
