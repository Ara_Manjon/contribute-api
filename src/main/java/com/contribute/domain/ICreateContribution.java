package com.contribute.domain;

import com.contribute.controller.Request;

public interface ICreateContribution {
  void save(Request request);

  Object retrieveContributions();
}
