package com.contribute.domain;

import com.contribute.controller.Request;

import java.util.Objects;

public class Contribution {
  private final Request content;

  public Contribution(Request content) {
    this.content = content;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    Contribution that = (Contribution) o;
    return Objects.equals(content, that.content);
  }

  @Override
  public int hashCode() {
    return Objects.hash(content);
  }

  public String getContent() {
    return content.content;
  }
}
