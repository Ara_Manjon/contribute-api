package com.contribute.controller;

import com.contribute.controller.Contribution.ContributionController;
import com.contribute.domain.ContributionDTO;
import com.contribute.infrastructure.ContributionsDTO;
import com.contribute.application.Contribution.CreateContributionImpl;
import org.junit.Before;
import org.junit.Test;
import org.junit.Assert;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class ContributionControllerShould {

  @Mock
  CreateContributionImpl createContributionImpl;

  @InjectMocks
  private ContributionController controller;

  @Before
  public void setup() {
    MockitoAnnotations.openMocks(this);
  }

  @Test
  public void create_content_correctly() {
    String content = "content";
    Request request = new Request();
    request.content = content;

    ResponseEntity<Object> responseExpected = controller.save(request);

    verify(createContributionImpl).save(request);
    Assert.assertNotNull(responseExpected);
    Assert.assertEquals("Contribution created successfully", responseExpected.getBody());
    Assert.assertEquals(HttpStatus.CREATED, responseExpected.getStatusCode());
  }

  @Test
  public void throw_error_when_service_is_unavailable() {
    String content = "content";
    Request request = new Request();
    request.content = content;
    doThrow(new RuntimeException()).when(createContributionImpl).save(request);

    ResponseEntity<Object> responseExpected = controller.save(request);

    Assert.assertNotNull(responseExpected);
    Assert.assertEquals(HttpStatus.SERVICE_UNAVAILABLE, responseExpected.getStatusCode());
  }

  @Test
  public void retrieve_all_contributions_correctly() {
    String content1 = "content1";
    ContributionDTO contributionDTO1 = new ContributionDTO();
    contributionDTO1.content = content1;
    String content2 = "content2";
    ContributionDTO contributionDTO2 = new ContributionDTO();
    contributionDTO2.content = content2;
    ContributionsDTO expectedContributions = new ContributionsDTO();
    expectedContributions.add(contributionDTO1);
    expectedContributions.add(contributionDTO2);
    when(createContributionImpl.retrieveContributions()).thenReturn(expectedContributions);

    ResponseEntity<Object> responseExpected = controller.retrieveContributions();

    Assert.assertNotNull(responseExpected);
    Assert.assertEquals(HttpStatus.OK, responseExpected.getStatusCode());
  }

  @Test
  public void throw_error_when_service_is_unavailable_when_retrieve_all_contributions() {
    doThrow(new RuntimeException()).when(createContributionImpl).retrieveContributions();

    ResponseEntity<Object> responseExpected = controller.retrieveContributions();

    Assert.assertNotNull(responseExpected);
    Assert.assertEquals(HttpStatus.SERVICE_UNAVAILABLE, responseExpected.getStatusCode());
  }
}
