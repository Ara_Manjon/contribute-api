package com.contribute.infrastructure;

import com.contribute.controller.Request;
import com.contribute.domain.Contribution;
import com.contribute.domain.ContributionDTO;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
@Sql(scripts = "/schema.sql")
public class SQLRepositoryShould {

  @Autowired
  SQLRepository sqlRepository;

  @Test
  public void create_a_content_correctly() {
    String content = "content";
    Request request = new Request();
    request.content = content;
    Contribution contribution = new Contribution(request);
    ContributionDTO contributionDTO = new ContributionDTO();
    contributionDTO.content = content;
    ContributionsDTO expectedContributions = new ContributionsDTO();
    expectedContributions.add(contributionDTO);

    sqlRepository.create(contribution);

    Assert.assertEquals(expectedContributions, sqlRepository.findAll());
  }

  @Test
  public void retrieve_all_contributions_correctly() {
    String content1 = "content1";
    Request request1 = new Request();
    request1.content = content1;
    Contribution contribution1 = new Contribution(request1);
    ContributionDTO contributionDTO1 = new ContributionDTO();
    contributionDTO1.content = content1;
    String content2 = "content2";
    Request request2 = new Request();
    request2.content = content2;
    Contribution contribution2 = new Contribution(request2);
    ContributionDTO contributionDTO2 = new ContributionDTO();
    contributionDTO2.content = content2;
    ContributionsDTO expectedContributions = new ContributionsDTO();
    expectedContributions.add(contributionDTO1);
    expectedContributions.add(contributionDTO2);

    sqlRepository.create(contribution1);
    sqlRepository.create(contribution2);

    Assert.assertEquals(expectedContributions, sqlRepository.findAll());
  }
}
