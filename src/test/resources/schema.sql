DROP TABLE IF EXISTS contributions;

CREATE TABLE IF NOT EXISTS contributions (
   content VARCHAR(512) NOT NULL
);